/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.os;

interface IPeripheralManagerClient {
  // Gpio functions.
  void ListGpio(out @utf8InCpp List<String> gpios);

  void OpenGpio(@utf8InCpp String name);

  void ReleaseGpio(@utf8InCpp String name);

  void SetGpioEdge(@utf8InCpp String name, int type);

  void SetGpioActiveType(@utf8InCpp String name, int type);

  void SetGpioDirection(@utf8InCpp String name, int direction);

  void SetGpioValue(@utf8InCpp String name, boolean value);

  boolean GetGpioValue(@utf8InCpp String name);

  FileDescriptor GetGpioPollingFd(@utf8InCpp String name);


  // Spi functions.
  void ListSpiBuses(out @utf8InCpp List<String> buses);

  void OpenSpiDevice(@utf8InCpp String name);

  void ReleaseSpiDevice(@utf8InCpp String name);

  void SpiDeviceWriteByte(@utf8InCpp String name, byte data);

  void SpiDeviceWriteBuffer(@utf8InCpp String name, in byte[] buffer);

  void SpiDeviceTransfer(@utf8InCpp String name, in @nullable byte[] tx_data,
                         out @nullable byte[] rx_data, int len);

  void SpiDeviceSetFrequency(@utf8InCpp String name, int frequency_hz);

  void SpiDeviceSetBitJustification(@utf8InCpp String name,
                                    boolean lsb_first);

  void SpiDeviceSetMode(@utf8InCpp String name, int mode);

  void SpiDeviceSetBitsPerWord(@utf8InCpp String name, int nbits);

  void SpiDeviceSetDelay(@utf8InCpp String name, int delay_usecs);


  // Leds functions.
  void ListLeds(out @utf8InCpp List<String> leds);

  void OpenLed(@utf8InCpp String name);

  void ReleaseLed(@utf8InCpp String name);

  int LedGetBrightness(@utf8InCpp String name);

  int LedGetMaxBrightness(@utf8InCpp String name);

  void LedSetBrightness(@utf8InCpp String name, int brightness);


  // I2c functions.
  void ListI2cBuses(out @utf8InCpp List<String> buses);

  void OpenI2cDevice(@utf8InCpp String name, int address);

  void ReleaseI2cDevice(@utf8InCpp String name, int address);

  int I2cRead(@utf8InCpp String name, int address, out byte[] data, int size);

  int I2cReadRegByte(@utf8InCpp String name, int address, int reg);

  int I2cReadRegWord(@utf8InCpp String name, int address, int reg);

  int I2cReadRegBuffer(@utf8InCpp String name, int address, int reg,
                       out byte[] data, int size);

  int I2cWrite(@utf8InCpp String name, int address, in byte[] data);

  void I2cWriteRegByte(@utf8InCpp String name, int address, int reg, byte val);

  void I2cWriteRegWord(@utf8InCpp String name, int address, int reg, int val);

  int I2cWriteRegBuffer(@utf8InCpp String name, int address, int reg,
                        in byte[] data);

  // Uart functions
  void ListUartDevices(out @utf8InCpp List<String> devices);

  void OpenUartDevice(@utf8InCpp String name);

  void ReleaseUartDevice(@utf8InCpp String name);

  void SetUartDeviceBaudrate(@utf8InCpp String name, int baudrate);

  int UartDeviceWrite(@utf8InCpp String name, in byte[] data);

  int UartDeviceRead(@utf8InCpp String name, out byte[] data, int size);
}
