/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <unistd.h>

#include <memory>

#include <base/logging.h>
#include <brillo/flag_helper.h>

#include <peripheralmanager/peripheral_manager_client.h>

int main(int argc, char* argv[]) {
  DEFINE_string(pin, "", "Pin to toggle");
  brillo::FlagHelper::Init(argc, argv, "Example PeripheralManager client.");
  logging::InitLogging(logging::LoggingSettings());

  std::string pin_name = "IO13";
  if (!FLAGS_pin.empty())
    pin_name = FLAGS_pin;

  LOG(INFO) << "Toggling Pin " << pin_name;

  // Get a client to the PeripheralManager.
  BPeripheralManagerClient* client = BPeripheralManagerClient_new();

  if (!client) {
    LOG(ERROR) << "Failed to connect to client";
    return 1;
  }

  // Open GPIO pin.
  BGpio* my_gpio;
  int ret =
      BPeripheralManagerClient_openGpio(client, pin_name.c_str(), &my_gpio);
  if (ret) {
    LOG(ERROR) << "Failed to open Gpio: " << strerror(ret);
    return 1;
  }

  // Set the direction to out.
  ret = BGpio_setDirection(my_gpio, DIRECTION_OUT_INITIALLY_HIGH);
  if (ret) {
    LOG(ERROR) << "Failed to set gpio pin direction: " << strerror(ret);
    return 1;
  }

  // Toggle the output.
  BGpio_setValue(my_gpio, 0);
  sleep(1);
  BGpio_setValue(my_gpio, 1);

  // Release the gpio pin
  BGpio_delete(my_gpio);

  // Close the connection to PeripheralManager.
  BPeripheralManagerClient_delete(client);

  LOG(INFO) << "Exiting";
  return 0;
}
