#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(call my-dir)

# Flips a GPIO on and off.
# ========================================================

include $(CLEAR_VARS)
LOCAL_MODULE := peripheralman_gpio_flip
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := -Wall -Werror -Wno-unused-parameter
LOCAL_CFLAGS += -Wno-sign-promo  # for libchrome
LOCAL_SHARED_LIBRARIES := \
  libbrillo \
  libchrome \
  libperipheralman \
  libutils \

LOCAL_SRC_FILES := \
  gpio_flip_example.cc \

include $(BUILD_EXECUTABLE)

# Watches the state of a gpio with interrupts.
# ========================================================

include $(CLEAR_VARS)
LOCAL_MODULE := peripheralman_gpio_interrupt
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := -Wall -Werror -Wno-unused-parameter
LOCAL_CFLAGS += -Wno-sign-promo  # for libchrome
LOCAL_SHARED_LIBRARIES := \
  libbrillo \
  libchrome \
  libperipheralman \
  libutils \

LOCAL_SRC_FILES := \
  gpio_interrupt_example.cc \

include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := pio_flash_apa10c
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := -Wall -Werror -Wno-unused-parameter
LOCAL_CFLAGS += -Wno-sign-promo  # for libchrome
LOCAL_SHARED_LIBRARIES := \
  libbrillo \
  libchrome \
  libperipheralman \
  libutils \

LOCAL_SRC_FILES := \
  pio_flash_apa10c.cc \

include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := peripheralman_blink_led
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := -Wall -Werror -Wno-unused-parameter
LOCAL_CFLAGS += -Wno-sign-promo  # for libchrome
LOCAL_SHARED_LIBRARIES := \
  libbrillo \
  libchrome \
  libperipheralman \
  libutils \

LOCAL_SRC_FILES := \
  led_example.cc \

include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := pio_mcp9808
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := -Wall -Werror -Wno-unused-parameter
LOCAL_CFLAGS += -Wno-sign-promo  # for libchrome
LOCAL_SHARED_LIBRARIES := \
  libbrillo \
  libchrome \
  libperipheralman \
  libutils \

LOCAL_SRC_FILES := \
  pio_mcp9808.cc \

include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := pio_gy521
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := -Wall -Werror -Wno-unused-parameter
LOCAL_CFLAGS += -Wno-sign-promo  # for libchrome
LOCAL_SHARED_LIBRARIES := \
  libbrillo \
  libchrome \
  libperipheralman \
  libutils \

LOCAL_SRC_FILES := \
  pio_gy521.cc \

include $(BUILD_EXECUTABLE)

# Check that headers are C89 compatible.
include $(CLEAR_VARS)
LOCAL_MODULE := compatibility_test
LOCAL_CFLAGS := -Wall -Wextra -std=c89 -pedantic -Wmissing-prototypes \
  -Wstrict-prototypes -Wold-style-definition -Wno-comment

LOCAL_SHARED_LIBRARIES := libperipheralman
LOCAL_STATIC_LIBRARIES := peripheral_manager_hal_headers

LOCAL_SRC_FILES := headers_are_c89.c
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := pio_uart
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := -Wall -Werror -Wno-unused-parameter
LOCAL_CFLAGS += -Wno-sign-promo  # for libchrome
LOCAL_SHARED_LIBRARIES := \
  libbrillo \
  libchrome \
  libperipheralman \
  libutils \

LOCAL_SRC_FILES := \
  pio_uart.cc \

include $(BUILD_EXECUTABLE)
