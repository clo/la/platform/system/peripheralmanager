/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "fake_devices.h"

#include <stdio.h>

namespace android {

FakeCharDevice::FakeCharDevice() {}
FakeCharDevice::~FakeCharDevice() {}

// TODO(leecam): Implement these.
int FakeCharDevice::Open(const char* pathname, int flags) {
  return 1;
}

int FakeCharDevice::Close(int fd) {
  return 0;
}

int FakeCharDevice::Ioctl(int fd, int request, void* argp) {
  return 0;
}

ssize_t FakeCharDevice::Read(int fd, void* buf, size_t count) {
  return count;
}

ssize_t FakeCharDevice::Write(int fd, const void* buf, size_t count) {
  return count;
}

int FakeCharDevice::Poll(struct pollfd* fds, nfds_t nfds, int timeout) {
  return 0;
}

FakeDeviceFactory::FakeDeviceFactory() {}

FakeDeviceFactory::~FakeDeviceFactory() {}

std::unique_ptr<CharDeviceInterface> FakeDeviceFactory::NewCharDevice() {
  return std::unique_ptr<CharDeviceInterface>(new FakeCharDevice());
}

}  // namespace android
