/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SYSTEM_PERIPHERALMANAGER_DAEMON_UART_MANAGER_H_
#define SYSTEM_PERIPHERALMANAGER_DAEMON_UART_MANAGER_H_

#include <stdint.h>

#include <map>
#include <memory>
#include <string>
#include <vector>

#include <base/macros.h>

#include "pin_mux_manager.h"
#include "uart_driver.h"

namespace android {

struct UartSysfs {
  std::string name;
  std::string path;
  std::string mux;
  std::unique_ptr<UartDriverInterface> driver_;
};

class UartDevice {
 public:
  explicit UartDevice(UartSysfs* uart_device) : uart_device_(uart_device) {}
  ~UartDevice() { uart_device_->driver_.reset(); }

  int SetBaudrate(uint32_t baudrate) {
    return uart_device_->driver_->SetBaudrate(baudrate);
  }

  int Write(const std::vector<uint8_t>& data, uint32_t* bytes_written) {
    return uart_device_->driver_->Write(data, bytes_written);
  }

  int Read(std::vector<uint8_t>* data, uint32_t size, uint32_t* bytes_read) {
    return uart_device_->driver_->Read(data, size, bytes_read);
  }

 private:
  UartSysfs* uart_device_;
};

class UartManager {
 public:
  ~UartManager();

  // Get the singleton.
  static UartManager* GetManager();
  static void ResetManager();

  // Used by the BSP to tell PMan of an sysfs uart_device.
  bool RegisterUartDevice(const std::string& name, const std::string& path);
  bool SetPinMux(const std::string& name, const std::string& mux);

  std::vector<std::string> GetDevicesList();
  bool HasUartDevice(const std::string& name);

  bool RegisterDriver(std::unique_ptr<UartDriverInfoBase> driver_info);

  std::unique_ptr<UartDevice> OpenUartDevice(const std::string& name);

 private:
  UartManager();

  std::map<std::string, std::unique_ptr<UartDriverInfoBase>> driver_infos_;
  std::map<std::string, UartSysfs> uart_devices_;

  DISALLOW_COPY_AND_ASSIGN(UartManager);
};

}  // namespace android

#endif  // SYSTEM_PERIPHERALMANAGER_DAEMON_UART_MANAGER_H_
