/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "led_manager.h"

#include <base/logging.h>

namespace android {

std::unique_ptr<LedManager> g_led_manager;

LedManager::LedManager() {}

LedManager::~LedManager() {}

// static
LedManager* LedManager::GetLedManager() {
  if (!g_led_manager) {
    g_led_manager.reset(new LedManager());
  }
  return g_led_manager.get();
}

// static
void LedManager::ResetLedManager() {
  g_led_manager.reset();
}

bool LedManager::RegisterDriver(
    std::unique_ptr<LedDriverInfoBase> driver_info) {
  std::string key = driver_info->Compat();
  driver_infos_[key] = std::move(driver_info);
  return true;
}

bool LedManager::RegisterLedSysfs(const std::string& name,
                                  const std::string& led_name) {
  if (leds_.count(name))
    return false;
  leds_[name].name = led_name;
  return true;
}

std::unique_ptr<Led> LedManager::OpenLed(const std::string& name) {
  auto led_it = leds_.find(name);
  if (led_it == leds_.end()) {
    LOG(WARNING) << "LedManager: Led not found. " << name;
    return nullptr;
  }

  // Check its not alread in use
  if (led_it->second.driver_) {
    LOG(WARNING) << "LedManager: Led in use. " << name;
    return nullptr;
  }

  // Find a driver.
  auto driver_info_it = driver_infos_.find("LEDSYSFS");

  // Fail if there is no driver.
  if (driver_info_it == driver_infos_.end()) {
    LOG(WARNING) << "LedManager: Failed to find driver " << name;
    return nullptr;
  }

  std::unique_ptr<LedDriverInterface> driver(driver_info_it->second->Probe());

  // Set pin mux
  if (!led_it->second.mux.empty()) {
    // TODO(leecam): Enable pin muxing
  }

  if (!driver->Init(led_it->second.name)) {
    LOG(WARNING) << "LedManager: Failed to init driver " << name;
    return nullptr;
  }

  led_it->second.driver_ = std::move(driver);

  return std::unique_ptr<Led>(new Led(&(led_it->second)));
}

std::vector<std::string> LedManager::GetLeds() {
  std::vector<std::string> leds;
  for (auto& i : leds_)
    leds.push_back(i.first);
  return leds;
}

bool LedManager::HasLed(const std::string& name) {
  return leds_.count(name);
}

}  // namespace android
