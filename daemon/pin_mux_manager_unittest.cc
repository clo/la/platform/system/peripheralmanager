/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>

#include <gtest/gtest.h>

#include "pin_mux_manager.h"

namespace android {

class PinMuxManagerTest : public ::testing::Test {
 public:
  PinMuxManagerTest() {}
  ~PinMuxManagerTest() = default;

 protected:
  PinMuxManager manager;
  pin_mux_callbacks callbacks = {nullptr, nullptr};
};

TEST_F(PinMuxManagerTest, RegisterPin) {
  ASSERT_TRUE(manager.RegisterPin("IO1", true, callbacks));
  ASSERT_TRUE(manager.RegisterPin("IO2", true, callbacks));

  ASSERT_FALSE(manager.RegisterPin("IO1", true, callbacks));
  ASSERT_FALSE(manager.RegisterPin("IO2", true, callbacks));
}

TEST_F(PinMuxManagerTest, RegisterGroup) {
  ASSERT_TRUE(manager.RegisterPin("IO1", true, callbacks));
  ASSERT_TRUE(manager.RegisterPin("IO2", true, callbacks));
  ASSERT_TRUE(manager.RegisterPin("IO3", true, callbacks));

  ASSERT_TRUE(manager.RegisterPinGroup("Group1", {"IO1", "IO2", "IO3"}));
  ASSERT_TRUE(manager.RegisterPinGroup("Group2", {"IO1"}));
  ASSERT_FALSE(manager.RegisterPinGroup("Group1", {"IO1", "IO2", "IO3"}));
  ASSERT_FALSE(manager.RegisterPinGroup("Group3", {"IO1", "BAD", "IO3"}));
}

TEST_F(PinMuxManagerTest, RegisterSource) {
  ASSERT_TRUE(manager.RegisterPin("IO1", true, callbacks));
  ASSERT_TRUE(manager.RegisterPin("IO2", true, callbacks));
  ASSERT_TRUE(manager.RegisterPin("IO3", true, callbacks));

  ASSERT_TRUE(manager.RegisterPinGroup("Group1", {"IO1", "IO2", "IO3"}));
  ASSERT_TRUE(manager.RegisterPinGroup("Group2", {"IO1"}));

  ASSERT_TRUE(manager.RegisterSource("Source1", {"Group1"}));
  ASSERT_TRUE(manager.RegisterSource("Source2", {"Group1", "Group2"}));

  ASSERT_FALSE(manager.RegisterSource("Source3", {"Bad"}));

  ASSERT_FALSE(manager.RegisterSource("Source1", {"Group1"}));
}

TEST_F(PinMuxManagerTest, SetSource) {
  ASSERT_TRUE(manager.RegisterPin("IO1", true, callbacks));
  ASSERT_TRUE(manager.RegisterPin("IO2", true, callbacks));
  ASSERT_TRUE(manager.RegisterPin("IO3", true, callbacks));
  ASSERT_TRUE(manager.RegisterPin("IO4", true, callbacks));

  ASSERT_TRUE(manager.RegisterPinGroup("Group1", {"IO1", "IO2", "IO3"}));
  ASSERT_TRUE(manager.RegisterPinGroup("Group2", {"IO4"}));

  ASSERT_TRUE(manager.RegisterSource("Source1", {"Group1", "Group2"}));

  ASSERT_TRUE(manager.SetSource("Source1", "Group1"));
  ASSERT_TRUE(manager.SetSource("Source1", "Group2"));
}

// TODO(leecam): Write lots more tetsts :)

}  // namespace android