/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SYSTEM_PERIPHERALMANAGER_DAEMON_GPIO_DRIVER_SYSFS_H_
#define SYSTEM_PERIPHERALMANAGER_DAEMON_GPIO_DRIVER_SYSFS_H_

#include <stdint.h>

#include <base/macros.h>

#include "gpio_driver.h"

namespace android {

class GpioDriverSysfs : public GpioDriverInterface {
 public:
  explicit GpioDriverSysfs(void* arg);
  ~GpioDriverSysfs();

  static std::string Compat() { return "GPIOSYSFS"; }

  bool Init(uint32_t index) override;

  // Gpio Driver interface.
  bool SetValue(bool val) override;
  bool GetValue(bool* val) override;
  bool SetActiveType(GpioActiveType type) override;
  bool SetDirection(GpioDirection direction) override;
  bool SetEdgeType(GpioEdgeType type) override;
  bool GetPollingFd(::android::base::unique_fd* fd) override;

 private:
  bool Enable();
  bool Disable();
  bool ExportGpio(uint32_t index);
  bool WriteToFile(const std::string& file, const std::string& value);
  bool ReadFromFile(const std::string& file, std::string* value);

  int fd_;

  DISALLOW_COPY_AND_ASSIGN(GpioDriverSysfs);
};

}  // namespace android

#endif  // SYSTEM_PERIPHERALMANAGER_DAEMON_GPIO_DRIVER_SYSFS_H_
