/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SYSTEM_PERIPHERALMANAGER_DAEMON_GPIO_DRIVER_MOCK_H_
#define SYSTEM_PERIPHERALMANAGER_DAEMON_GPIO_DRIVER_MOCK_H_

#include <stdint.h>

#include <base/macros.h>
#include <gmock/gmock.h>

#include "gpio_driver.h"

namespace android {

class GpioDriverMock : public GpioDriverInterface {
 public:
  explicit GpioDriverMock(void* arg) : is_input(true) {}
  ~GpioDriverMock() {}

  static std::string Compat() { return "GPIOSYSFS"; }

  bool Init(uint32_t index) { return true; }

  bool SetValue(bool val) { return !is_input; };

  bool GetValue(bool* val) { return true; };
  bool SetActiveType(GpioActiveType type) { return true; };

  bool SetDirection(GpioDirection direction) {
    is_input = direction == kDirectionIn;
    return true;
  };

  bool SetEdgeType(GpioEdgeType type) { return true; };
  bool GetPollingFd(::android::base::unique_fd* fd) { return true; };

 private:
  bool is_input;
  DISALLOW_COPY_AND_ASSIGN(GpioDriverMock);
};

}  // namespace android

#endif  // SYSTEM_PERIPHERALMANAGER_DAEMON_GPIO_DRIVER_MOCK_H_
