/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SYSTEM_PERIPHERALMANAGER_DAEMON_PERIPHERAL_MANAGER_CLIENT_H_
#define SYSTEM_PERIPHERALMANAGER_DAEMON_PERIPHERAL_MANAGER_CLIENT_H_

#include <map>
#include <memory>
#include <string>

#include <base/macros.h>

#include <android/os/BnPeripheralManagerClient.h>

#include "gpio_manager.h"
#include "i2c_manager.h"
#include "led_manager.h"
#include "spi_manager.h"
#include "uart_manager.h"

using android::binder::Status;
using android::os::BnPeripheralManagerClient;

namespace android {

class PeripheralManagerClient : public BnPeripheralManagerClient {
 public:
  PeripheralManagerClient();
  ~PeripheralManagerClient();

  // Gpio functions.
  Status ListGpio(std::vector<std::string>* gpios) override;

  Status OpenGpio(const std::string& name) override;

  Status ReleaseGpio(const std::string& name) override;

  Status SetGpioEdge(const std::string& name, int type) override;

  Status SetGpioActiveType(const std::string& name, int type) override;

  Status SetGpioDirection(const std::string& name,
                          int direction) override;

  Status SetGpioValue(const std::string& name, bool value) override;

  Status GetGpioValue(const std::string& name, bool* value) override;

  Status GetGpioPollingFd(const std::string& name,
                          ::android::base::unique_fd* fd) override;

  // Spi functions.
  Status ListSpiBuses(std::vector<std::string>* buses) override;

  Status OpenSpiDevice(const std::string& name) override;

  Status ReleaseSpiDevice(const std::string& name) override;

  Status SpiDeviceWriteByte(const std::string& name,
                            int8_t byte) override;

  Status SpiDeviceWriteBuffer(
      const std::string& name,
      const std::vector<uint8_t>& buffer) override;

  Status SpiDeviceTransfer(
      const std::string& name,
      const std::unique_ptr<std::vector<uint8_t>>& tx_data,
      std::unique_ptr<std::vector<uint8_t>>* rx_data,
      int len) override;

  Status SpiDeviceSetMode(const std::string& name, int mode) override;

  Status SpiDeviceSetFrequency(const std::string& name,
                               int frequency_hz) override;

  Status SpiDeviceSetBitJustification(const std::string& name,
                                      bool lsb_first) override;

  Status SpiDeviceSetBitsPerWord(const std::string& name,
                                 int nbits) override;

  Status SpiDeviceSetDelay(const std::string& name,
                           int delay_usecs) override;

  // Led functions.
  Status ListLeds(std::vector<std::string>* leds) override;

  Status OpenLed(const std::string& name) override;

  Status ReleaseLed(const std::string& name) override;

  Status LedGetBrightness(const std::string& name,
                          int* brightness) override;
  Status LedGetMaxBrightness(const std::string& name,
                             int* max_brightness) override;
  Status LedSetBrightness(const std::string& name,
                          int brightness) override;

  // I2c functions.
  Status ListI2cBuses(std::vector<std::string>* buses) override;

  Status OpenI2cDevice(const std::string& name,
                       int32_t address) override;

  Status ReleaseI2cDevice(const std::string& name,
                          int32_t address) override;

  Status I2cRead(const std::string& name,
                 int32_t address,
                 std::vector<uint8_t>* data,
                 int32_t size,
                 int32_t* bytes_read) override;

  Status I2cReadRegByte(const std::string& name,
                        int32_t address,
                        int32_t reg,
                        int32_t* val) override;

  Status I2cReadRegWord(const std::string& name,
                        int32_t address,
                        int32_t reg,
                        int32_t* val) override;

  Status I2cReadRegBuffer(const std::string& name,
                          int32_t address,
                          int32_t reg,
                          std::vector<uint8_t>* data,
                          int32_t size,
                          int32_t* bytes_read) override;

  Status I2cWrite(const std::string& name,
                  int32_t address,
                  const std::vector<uint8_t>& data,
                  int32_t* bytes_written) override;

  Status I2cWriteRegByte(const std::string& name,
                         int32_t address,
                         int32_t reg,
                         int8_t val) override;

  Status I2cWriteRegWord(const std::string& name,
                         int32_t address,
                         int32_t reg,
                         int32_t val) override;

  Status I2cWriteRegBuffer(const std::string& name,
                           int32_t address,
                           int32_t reg,
                           const std::vector<uint8_t>& data,
                           int32_t* bytes_written) override;

  // Uart functions.
  Status ListUartDevices(std::vector<std::string>* devices) override;

  Status OpenUartDevice(const std::string& name) override;

  Status ReleaseUartDevice(const std::string& name) override;

  Status SetUartDeviceBaudrate(const std::string& name,
                               int32_t baudrate) override;

  Status UartDeviceWrite(const std::string& name,
                         const std::vector<uint8_t>& data,
                         int* bytes_written) override;

  Status UartDeviceRead(const std::string& name,
                        std::vector<uint8_t>* data,
                        int size,
                        int* bytes_read) override;

 private:
  std::map<std::string, std::unique_ptr<GpioPin>> gpios_;
  std::map<std::pair<std::string, uint32_t>, std::unique_ptr<I2cDevice>>
      i2c_devices_;
  std::map<std::string, std::unique_ptr<SpiDevice>> spi_devices_;
  std::map<std::string, std::unique_ptr<Led>> leds_;
  std::map<std::string, std::unique_ptr<UartDevice>> uart_devices_;
  DISALLOW_COPY_AND_ASSIGN(PeripheralManagerClient);
};

}  // namespace android

#endif  // SYSTEM_PERIPHERALMANAGER_DAEMON_PERIPHERAL_MANAGER_CLIENT_H_
