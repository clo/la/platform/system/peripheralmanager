/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SYSTEM_PERIPHERALMANAGER_DAEMON_LED_DRIVER_SYSFS_H_
#define SYSTEM_PERIPHERALMANAGER_DAEMON_LED_DRIVER_SYSFS_H_

#include <stdint.h>

#include <base/macros.h>

#include "led_driver.h"

namespace android {

class LedDriverSysfs : public LedDriverInterface {
 public:
  explicit LedDriverSysfs(std::string* prefix);
  ~LedDriverSysfs();

  static std::string Compat() { return "LEDSYSFS"; }

  bool Init(const std::string& name) override;
  bool SetBrightness(uint32_t val) override;
  bool GetBrightness(uint32_t* val) override;
  bool GetMaxBrightness(uint32_t* val) override;

 private:
  bool ReadFromFile(const std::string& file, std::string* value);
  bool WriteToFile(const std::string& file, const std::string& value);

  int fd_;

  // Used for unit test only.
  // Ownership is in the test.
  std::string* prefix_;

  DISALLOW_COPY_AND_ASSIGN(LedDriverSysfs);
};

}  // namespace android

#endif  // SYSTEM_PERIPHERALMANAGER_DAEMON_LED_DRIVER_SYSFS_H_
