/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>

#include <gtest/gtest.h>

#include "gpio_driver_sysfs.h"
#include "gpio_manager.h"

namespace android {

class GpioManagerTest : public ::testing::Test {
 public:
  GpioManagerTest() {}
  ~GpioManagerTest() = default;

 protected:
  GpioManager manager;
};

TEST_F(GpioManagerTest, RegisterDriver) {
  manager.RegisterDriver(std::unique_ptr<GpioDriverInfoBase>(
      new GpioDriverInfo<GpioDriverSysfs, void*>(nullptr)));
}

TEST_F(GpioManagerTest, BasicOpen) {
  manager.RegisterDriver(std::unique_ptr<GpioDriverInfoBase>(
      new GpioDriverInfo<GpioDriverSysfs, void*>(nullptr)));

  manager.RegisterGpioSysfs("IO11", 40);

  std::unique_ptr<GpioPin> pin(manager.OpenGpioPin("IO11"));
}

// TODO(leecam): Once this stablizes and has a stub,
// write lots more tests.

}  // namespace android