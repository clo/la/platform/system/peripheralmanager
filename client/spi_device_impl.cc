/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vector>

#include <peripheralmanager/spi_device.h>

#include "spi_device_impl.h"

using android::sp;
using android::os::IPeripheralManagerClient;
using android::binder::Status;

SpiDeviceImpl::SpiDeviceImpl(const std::string& name,
                             sp<IPeripheralManagerClient> client)
    : name_(name), client_(client) {}

SpiDeviceImpl::~SpiDeviceImpl() {
  client_->ReleaseSpiDevice(name_);
}

int SpiDeviceImpl::WriteByte(uint8_t byte) {
  return client_->SpiDeviceWriteByte(name_, byte).serviceSpecificErrorCode();
}

int SpiDeviceImpl::WriteBuffer(const uint8_t* data, size_t len) {
  std::vector<uint8_t> v(data, data + len);
  return client_->SpiDeviceWriteBuffer(name_, v).serviceSpecificErrorCode();
}

int SpiDeviceImpl::Transfer(const uint8_t* tx_data,
                            uint8_t* rx_data,
                            size_t len) {
  std::unique_ptr<std::vector<uint8_t>> sent;
  std::unique_ptr<std::vector<uint8_t>> received;

  if (tx_data == nullptr)
    return EINVAL;

  sent.reset(new std::vector<uint8_t>(tx_data, tx_data + len));

  Status status = client_->SpiDeviceTransfer(name_, sent, &received, len);
  if (status.isOk() && rx_data)
    memcpy(rx_data, received->data(), len);
  return status.serviceSpecificErrorCode();
}

int SpiDeviceImpl::SetFrequency(uint32_t freq_hz) {
  return client_->SpiDeviceSetFrequency(name_, freq_hz)
      .serviceSpecificErrorCode();
}

int SpiDeviceImpl::SetMode(int mode) {
  return client_->SpiDeviceSetMode(name_, mode).serviceSpecificErrorCode();
}

int SpiDeviceImpl::SetBitJustification(int justification) {
  return client_
      ->SpiDeviceSetBitJustification(name_, justification == SPI_LSB_FIRST)
      .serviceSpecificErrorCode();
}

int SpiDeviceImpl::SetBitsPerWord(uint32_t bits_per_word) {
  return client_->SpiDeviceSetBitsPerWord(name_, bits_per_word)
      .serviceSpecificErrorCode();
}

int SpiDeviceImpl::SetDelay(uint16_t delay_usecs) {
  return client_->SpiDeviceSetDelay(name_, delay_usecs)
      .serviceSpecificErrorCode();
}
