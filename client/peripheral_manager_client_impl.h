/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SYSTEM_PERIPHERALMANAGER_PERIPHERAL_MANAGER_CLIENT_IMPL_H_
#define SYSTEM_PERIPHERALMANAGER_PERIPHERAL_MANAGER_CLIENT_IMPL_H_

#include <android/os/IPeripheralManagerClient.h>
#include <utils/StrongPointer.h>

#include "gpio_impl.h"
#include "i2c_device_impl.h"
#include "led_impl.h"
#include "spi_device_impl.h"
#include "uart_device_impl.h"

class PeripheralManagerClientImpl {
 public:
  PeripheralManagerClientImpl() = default;

  bool Init();

  int OpenGpio(const std::string& name, std::unique_ptr<GpioImpl>* gpio);
  int ListGpio(std::vector<std::string>* gpios);

  int OpenSpiDevice(const std::string& name,
                    std::unique_ptr<SpiDeviceImpl>* device);
  int ListSpiBuses(std::vector<std::string>* buses);

  int OpenLed(const std::string& name, std::unique_ptr<LedImpl>* device);
  int ListLeds(std::vector<std::string>* leds);

  int OpenI2cDevice(const std::string& name,
                    uint32_t address,
                    std::unique_ptr<I2cDeviceImpl>* device);
  int ListI2cBuses(std::vector<std::string>* buses);

  int OpenUartDevice(const std::string& name,
                     std::unique_ptr<UartDeviceImpl>* device);
  int ListUartDevices(std::vector<std::string>* buses);

 private:
  android::sp<android::os::IPeripheralManagerClient> client_;
  android::sp<android::IBinder> lifeline_;
};

#endif  // SYSTEM_PERIPHERALMANAGER_PERIPHERAL_MANAGER_CLIENT_IMPL_H_
