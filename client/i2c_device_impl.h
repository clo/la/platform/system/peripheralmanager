/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SYSTEM_PERIPHERALMANAGER_I2C_DEVICE_IMPL_H_
#define SYSTEM_PERIPHERALMANAGER_I2C_DEVICE_IMPL_H_

#include <string>

#include <android/os/IPeripheralManagerClient.h>
#include <utils/StrongPointer.h>

class I2cDeviceImpl {
 public:
  I2cDeviceImpl(const std::string& bus,
                uint32_t address,
                android::sp<android::os::IPeripheralManagerClient> client);

  int Read(void* data, uint32_t len, uint32_t* bytes_read);
  int ReadRegByte(uint8_t reg, uint8_t* val);
  int ReadRegWord(uint8_t reg, uint16_t* val);
  int ReadRegBuffer(uint8_t reg,
                    void* data,
                    uint32_t len,
                    uint32_t* bytes_read);

  int Write(const void* data, uint32_t len, uint32_t* bytes_written);
  int WriteRegByte(uint8_t reg, uint8_t byte);
  int WriteRegWord(uint8_t reg, uint16_t byte);
  int WriteRegBuffer(uint8_t reg,
                     const void* data,
                     uint32_t len,
                     uint32_t* bytes_written);

  ~I2cDeviceImpl();

 private:
  std::string name_;
  uint32_t address_;
  android::sp<android::os::IPeripheralManagerClient> client_;
};

#endif  // SYSTEM_PERIPHERALMANAGER_I2C_DEVICE_IMPL_H_
