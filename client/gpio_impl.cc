/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gpio_impl.h"

#include <android-base/unique_fd.h>
#include <binder/Status.h>

#include "peripheralmanager/constants.h"
#include "peripheralmanager/gpio.h"

using android::binder::Status;
using android::os::IPeripheralManagerClient;
using android::GpioActiveType;
using android::GpioDirection;
using android::GpioEdgeType;

bool ActiveTypeFromInt(int type, GpioActiveType* out) {
  if (type == ACTIVE_LOW) {
    *out = android::kActiveLow;
    return true;
  } else if (type == ACTIVE_HIGH) {
    *out = android::kActiveHigh;
    return true;
  }
  return false;
}

bool DirectionFromInt(int direction, android::GpioDirection* out) {
  switch (direction) {
    case DIRECTION_IN:
      *out = android::kDirectionIn;
      return true;
    case DIRECTION_OUT_INITIALLY_HIGH:
      *out = android::kDirectionOutInitiallyHigh;
      return true;
    case DIRECTION_OUT_INITIALLY_LOW:
      *out = android::kDirectionOutInitiallyLow;
      return true;
  }
  return false;
}

bool EdgeTypeFromInt(int type, android::GpioEdgeType* out) {
  switch (type) {
    case NONE_EDGE:
      *out = android::kEdgeNone;
      return true;
    case RISING_EDGE:
      *out = android::kEdgeRising;
      return true;
    case FALLING_EDGE:
      *out = android::kEdgeFalling;
      return true;
    case BOTH_EDGE:
      *out = android::kEdgeBoth;
      return true;
  }
  return false;
}

GpioImpl::GpioImpl(const std::string name,
                   android::sp<IPeripheralManagerClient> client)
    : name_(name), client_(client) {}

GpioImpl::~GpioImpl() {
  client_->ReleaseGpio(name_);
}

int GpioImpl::SetDirection(GpioDirection direction) {
  return client_->SetGpioDirection(name_, direction).serviceSpecificErrorCode();
}

int GpioImpl::SetEdgeTriggerType(GpioEdgeType type) {
  return client_->SetGpioEdge(name_, type).serviceSpecificErrorCode();
}

int GpioImpl::SetActiveType(GpioActiveType type) {
  return client_->SetGpioActiveType(name_, type).serviceSpecificErrorCode();
}

int GpioImpl::SetValue(int value) {
  return client_->SetGpioValue(name_, value != 0).serviceSpecificErrorCode();
}

int GpioImpl::GetValue(int* value) {
  bool val = true;
  Status ret = client_->GetGpioValue(name_, &val);
  if (ret.isOk()) {
    *value = val ? 1 : 0;
  }
  return ret.serviceSpecificErrorCode();
}

int GpioImpl::GetPollingFd(int* fd) {
  android::base::unique_fd scoped_fd;
  Status ret = client_->GetGpioPollingFd(name_, &scoped_fd);
  if (ret.isOk()) {
    uint8_t buf[2];
    read(scoped_fd.get(), buf, 2);
    lseek(scoped_fd.get(), 0, SEEK_SET);
    *fd = scoped_fd.release();
  }
  return ret.serviceSpecificErrorCode();
}
