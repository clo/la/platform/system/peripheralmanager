/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vector>

#include <peripheralmanager/i2c_device.h>

#include "i2c_device_impl.h"

using android::sp;
using android::os::IPeripheralManagerClient;
using android::binder::Status;

I2cDeviceImpl::I2cDeviceImpl(const std::string& name,
                             uint32_t address,
                             sp<IPeripheralManagerClient> client)
    : name_(name), address_(address), client_(client) {}

I2cDeviceImpl::~I2cDeviceImpl() {
  client_->ReleaseI2cDevice(name_, address_);
}

int I2cDeviceImpl::Read(void* data, uint32_t len, uint32_t* bytes_read) {
  std::vector<uint8_t> read;
  Status status = client_->I2cRead(
      name_, address_, &read, len, reinterpret_cast<int32_t*>(bytes_read));
  memcpy(data, read.data(), *bytes_read);
  return status.serviceSpecificErrorCode();
}

int I2cDeviceImpl::ReadRegByte(uint8_t reg, uint8_t* val) {
  int32_t tmp_val;
  int ret = client_->I2cReadRegByte(name_, address_, reg, &tmp_val)
                .serviceSpecificErrorCode();
  if (!ret) {
    *val = tmp_val;
  }
  return ret;
}

int I2cDeviceImpl::ReadRegWord(uint8_t reg, uint16_t* val) {
  int32_t tmp_val;
  int ret = client_->I2cReadRegWord(name_, address_, reg, &tmp_val)
                .serviceSpecificErrorCode();
  if (!ret) {
    *val = tmp_val;
  }
  return ret;
}

int I2cDeviceImpl::ReadRegBuffer(uint8_t reg,
                                 void* data,
                                 uint32_t len,
                                 uint32_t* bytes_read) {
  std::vector<uint8_t> read(len);
  Status status = client_->I2cReadRegBuffer(
      name_, address_, reg, &read, len, reinterpret_cast<int32_t*>(bytes_read));
  memcpy(data, read.data(), *bytes_read);

  return status.serviceSpecificErrorCode();
}

int I2cDeviceImpl::Write(const void* data,
                         uint32_t len,
                         uint32_t* bytes_written) {
  const uint8_t* bytes = reinterpret_cast<const uint8_t*>(data);
  std::vector<uint8_t> v(bytes, bytes + len);
  return client_
      ->I2cWrite(name_, address_, v, reinterpret_cast<int32_t*>(bytes_written))
      .serviceSpecificErrorCode();
}

int I2cDeviceImpl::WriteRegByte(uint8_t reg, uint8_t val) {
  return client_->I2cWriteRegByte(name_, address_, reg, val)
      .serviceSpecificErrorCode();
}

int I2cDeviceImpl::WriteRegWord(uint8_t reg, uint16_t val) {
  return client_->I2cWriteRegWord(name_, address_, reg, val)
      .serviceSpecificErrorCode();
}

int I2cDeviceImpl::WriteRegBuffer(uint8_t reg,
                                  const void* data,
                                  uint32_t len,
                                  uint32_t* bytes_written) {
  const uint8_t* bytes = reinterpret_cast<const uint8_t*>(data);
  std::vector<uint8_t> v(bytes, bytes + len);
  return client_
      ->I2cWriteRegBuffer(
          name_, address_, reg, v, reinterpret_cast<int32_t*>(bytes_written))
      .serviceSpecificErrorCode();
}
