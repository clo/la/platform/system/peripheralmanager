/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <led_impl.h>

using android::binder::Status;
using android::os::IPeripheralManagerClient;
using android::sp;

LedImpl::LedImpl(const std::string& name, sp<IPeripheralManagerClient> client)
    : name_(name), client_(client) {}

LedImpl::~LedImpl() {
  client_->ReleaseLed(name_);
}

int LedImpl::GetBrightness(uint32_t* brightness) {
  int val;
  Status status = client_->LedGetBrightness(name_, &val);
  if (status.isOk())
    *brightness = static_cast<uint32_t>(val);

  return status.serviceSpecificErrorCode();
}

int LedImpl::GetMaxBrightness(uint32_t* max_brightness) {
  int val;
  Status status = client_->LedGetMaxBrightness(name_, &val);
  if (status.isOk())
    *max_brightness = static_cast<uint32_t>(val);

  return status.serviceSpecificErrorCode();
}

int LedImpl::SetBrightness(uint32_t brightness) {
  return client_->LedSetBrightness(name_, brightness)
      .serviceSpecificErrorCode();
}
