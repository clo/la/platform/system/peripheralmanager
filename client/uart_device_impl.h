/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SYSTEM_PERIPHERALMANAGER_UART_DEVICE_IMPL_H_
#define SYSTEM_PERIPHERALMANAGER_UART_DEVICE_IMPL_H_

#include <string>

#include <android/os/IPeripheralManagerClient.h>
#include <utils/StrongPointer.h>

class UartDeviceImpl {
 public:
  UartDeviceImpl(const std::string& name,
                 android::sp<android::os::IPeripheralManagerClient> client);

  ~UartDeviceImpl();

  int SetBaudrate(uint32_t baudrate);

  int Write(const void* data, uint32_t size, uint32_t* bytes_written);

  int Read(void* data, uint32_t size, uint32_t* bytes_read);

 private:
  std::string name_;
  android::sp<android::os::IPeripheralManagerClient> client_;
};

#endif  // SYSTEM_PERIPHERALMANAGER_UART_DEVICE_IMPL_H_
